package fr.tfabre.WordPair;

import java.io.FileNotFoundException;

public class Main
{
    public static void main(String args[])
    {
        try
        {
            SizedWordList dic = new SizedWordList("wordlist.txt", 6);
            WordMatcher match1 = new WordMatcher(dic.getWordListOfSize(6),
                                                 dic.getWordListOfSize(1),
                                                 dic.getWordListOfSize(5));
            WordMatcher match2 = new WordMatcher(dic.getWordListOfSize(6),
                                                 dic.getWordListOfSize(2),
                                                 dic.getWordListOfSize(4));
            WordMatcher match3 = new WordMatcher(dic.getWordListOfSize(6),
                                                 dic.getWordListOfSize(3),
                                                 dic.getWordListOfSize(3));

            match1.showWordsWhoMatch();
            match2.showWordsWhoMatch();
            match3.showWordsWhoMatch();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}
