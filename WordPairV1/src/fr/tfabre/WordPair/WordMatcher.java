package fr.tfabre.WordPair;

import java.util.List;

/**
 * @brief From 2 subwords lists match the combination of the subword with
 * reference list
 */
public class WordMatcher
{
    private List<String> reference;
    private List<String> subWord1;
    private List<String> subWord2;
    private int length1 = 0;
    private int length2 = 0;
    private boolean canMatch = true;

    /**
     * @brief Construct WordMatcher object with sublists and reference list
     *
     * @param reference Words list of reference
     * @param subWord1 Words list to combine with the second to obtain one word
     *        of the reference list
     * @param subWord2 Words list to combine with the first to obtain one word
     *        of the reference list
     *
     * @note If one of the list is empty or if the length of the word of the
     *       first sublist plus the length of the word of the second first list
             is not equal to the length of word of the reference list the
             matcher do not compute anything
     * @attention All word from a subwords list must have the same size
     */
    public WordMatcher(List<String> reference, List<String> subWord1,
                       List<String> subWord2)
    {
        if (subWord1.size() == 0 || subWord2.size() == 0 ||
            reference.size() == 0)
            canMatch = false;

        else if (subWord1.get(0).length() + subWord2.get(0).length()
            != reference.get(0).length())
            canMatch = false;

        else
        {
            this.length1 = subWord1.get(0).length();
            this.length2 = subWord2.get(0).length();
        }

        this.reference = reference;
        this.subWord1 = subWord1;
        this.subWord2 = subWord2;
    }

    /**
     * @brief Compute combination of the subwords and display it if it matchs
     *        with the reference list
     */
    public void showWordsWhoMatch()
    {
        if (!canMatch)
            return;

        for (String word: reference)
        {
            String sub1 = word.substring(0, length1);
            String sub2 = word.substring(length1);
            if (subWord1.contains(sub1) && subWord2.contains(sub2))
                System.out.println(sub1 + " + " + sub2 + " => " + word);

            if (length1 != length2)
            {
                sub1 = word.substring(0, length2);
                sub2 = word.substring(length2);
                if (subWord2.contains(sub1) && subWord1.contains(sub2))
                    System.out.println(sub1 + " + " + sub2 + " => " + word);

            }
        }
    }
}
