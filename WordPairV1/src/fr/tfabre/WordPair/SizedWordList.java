package fr.tfabre.WordPair;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * @brief Contains words lists.
 *
 * All words lists contains words with single specific size. The max word size
 * is specified at the object build. The lists of words have words from size 1
 * to max word size previously precised.
 */ 
public class SizedWordList
{
    private List<List<String>> words;

    /**
     * @brief Build lists of word from file which have the same size
     *
     * @param filename File where the words come from
     * @param wordMaxLength The max word length for the sublists
     */
    public SizedWordList(String filename, int wordMaxLength)
        throws FileNotFoundException
    {
        this.words = new LinkedList<List<String>>();
        for (int i = 0; i < wordMaxLength; ++i)
            this.words.add(new LinkedList<String>());

        File file = new File(filename);
        FileInputStream inputStream = new FileInputStream(file);
        InputStreamReader streamReader = new InputStreamReader(inputStream);
        BufferedReader buff = new BufferedReader(streamReader);
        String line = null;
        try
        {
            while ((line = buff.readLine()) != null)
                if (!line.isEmpty())
                    skimAndFillWords(line);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * @brief Extract the word from the line and put it in the appropriate sublist
     *
     * @param line The line that contains word to extract
     */
    private void skimAndFillWords(String line)
    {
        String[] word = line.split("\\s+");
        if (word.length != 0)
            for (int i = 0; i < this.words.size(); ++i)
                if (word[0].length() == i +1)
                    this.words.get(i).add(word[0]);
    }

    /**
     * @brief Return a sublist of words of the size corresponding the value given
     *        in parameter
     *
     * @param len Size of word of the sublist
     * @return The sublist
     */
    public List<String> getWordListOfSize(int len)
    {
        if (len < 0 || len > this.words.size())
            throw new NoSuchElementException();
        return this.words.get(len -1);
    }
}
