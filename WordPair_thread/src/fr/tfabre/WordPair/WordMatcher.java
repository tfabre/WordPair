package fr.tfabre.WordPair;

import java.util.List;
import java.util.LinkedList;

/**
 * @brief From 2 subwords lists match the combination of the subword with
 * reference list
 */
public class WordMatcher extends Thread
{
    private List<String> reference;
    private List<String> subWord1;
    private List<String> subWord2;
    private boolean canMatch = true;
    private List<String> result;

    /**
     * @brief Construct WordMatcher object with sublists and reference list
     *
     * @param reference Words list of reference
     * @param subWord1 Words list to combine with the second to obtain one word
     *        of the reference list
     * @param subWord2 Words list to combine with the first to obtain one word
     *        of the reference list
     *
     * @note If one of the list is empty or if the length of the word of the
     *       first sublist plus the length of the word of the second first list
     *       is not equal to the length of word of the reference list the
     *       matcher do not compute anything
     * @attention All word from a subwords list must have the same size
     */
    public WordMatcher(List<String> reference, List<String> subWord1,
                       List<String> subWord2)
    {
        if (subWord1.size() == 0 || subWord2.size() == 0 ||
            reference.size() == 0)
            canMatch = false;

        else if (subWord1.get(0).length() + subWord2.get(0).length()
            != reference.get(0).length())
            canMatch = false;

        this.reference = reference;
        this.subWord1 = subWord1;
        this.subWord2 = subWord2;
        this.result = new LinkedList<String>();
    }

    /**
     * @brief Compute combination of the subwords and store it if it matchs
     *        with the reference list
     */
    public void run()
    {
        if (!canMatch)
            return;

        for (String w1: subWord1)
            for (String w2: subWord2)
                for (String word: reference)
                {
                    if ((w1 + w2).equals(word))
                        this.result.add(w1 + " + " + w2 + " => " + word);
                    else if ((w2 + w1).equals(word))
                        this.result.add(w2 + " + " + w1 + " => " + word);
                }
    }

    public List<String> getResult()
    {
        return this.result;
    }
}
