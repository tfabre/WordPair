package fr.tfabre.WordPair;

import java.io.FileNotFoundException;

public class Main
{
    public static void main(String args[])
    {
        try
        {
            SizedWordList dic = new SizedWordList("wordlist.txt", 6);
            WordMatcher match1 = new WordMatcher(dic.getWordListOfSize(6),
                                                 dic.getWordListOfSize(1),
                                                 dic.getWordListOfSize(5));
            WordMatcher match2 = new WordMatcher(dic.getWordListOfSize(6),
                                                 dic.getWordListOfSize(2),
                                                 dic.getWordListOfSize(4));
            WordMatcher match3 = new WordMatcher(dic.getWordListOfSize(6),
                                                 dic.getWordListOfSize(3),
                                                 dic.getWordListOfSize(3));

            match1.start();
            match2.start();
            match3.start();

            match1.join();
            match2.join();
            match3.join();

            for (String s: match1.getResult())
                System.out.println(s);

            for (String s: match2.getResult())
                System.out.println(s);

            for (String s: match3.getResult())
                System.out.println(s);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            System.err.println(e.getMessage());
        }
    }
}
